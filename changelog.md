# Changelog
All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [2.10.2] - 2021.04.14

### Changed

- added 11 new item VAT type
    - src/szamlaagent/item/Item.php

## [2.10.1] - 2021.03.24

### Changed

- we will not send notification email, if the Buyer.sendEmail property is not set
    - src/szamlaagent/Buyer.php
    - src/szamlaagent/BuyerLedger.php
    
- The last two parameters of the InvoiceItemLedger constructor can also be numbers
    - src/szamlaagent/ledger/InvoiceItemLedger.php
    
````
    $itemLedger = new InvoiceItemLedger('economic event type', 'vat economic event type', 123, 467);
````

## [2.10.0] - 2021.03.10

### Added

- added changelog to the package
    - src/szamlaagent/changelog.md 

## [2.9.10] - 2021.01.27

### Added

- added new function: invoice PDF preview creation
    - src/szamlaagent/header/InvoiceHeader.php

````
    $invoice = new Invoice(Invoice::INVOICE_TYPE_E_INVOICE);  
    $invoice->getHeader()->setPreviewPdf(true);
````

- added new function: Turn off default PDF file saving (you can save the preview PDF yourself)
    - src/szamlaagent/SzamlaAgent.php

````
    $agent = SzamlaAgentAPI::create('agentApiKey');  
    $agent->setPdfFileSave(false);
````

### Changed

- TaxPayer interface update (NAV 3.0 format). If you are building business logic on this interface, you recommend using your own XML processor to handle the data coming from NAV.
    - src/szamlaagent/response/TaxPayerResponse.php
    - src/szamlaagent/response/SzamlaAgentResponse.php


### Deprecated

- Processing of data received via the NAV TaxPayer interface via the TaxPayerResponse object was supported until version 2.9.10.
    - src/szamlaagent/response/TaxPayerResponse.php

## [2.9.9] - 2020.12.10

### Added

- added new VAT codes (TAHK, TEHK) 
    - src/szamlaagent/item/Item.php

### Changed

- XML creation logging update (if the creation fails, the generated XML is logged)
    - src/szamlaagent/SzamlaAgentRequest.php
    - src/szamlaagent/SzamlaAgentException.php

## [2.9.8] - 2020.11.24

### Fixed

- TaxPayer interface bugfix (& character handing in taxpayerName field)
    - src/szamlaagent/SimpleXMLExtended.php (cleanXMLNode)
    
## [2.9.7] - 2020.10.07

### Added

- added new function: corrective and reverse invoice buyer taxnumber setting
    - src/szamlaagent/Buyer.php
    
## [2.9.6] - 2020.09.16

### Changed

- Taxpayer options changed
    - src/szamlaagent/TaxPayer.php
    
- The name of the generated XML file contains the full timestamp
    - src/szamlaagent/SzamlaAgentUtil.php (getDateTimeWithMilliseconds) 

### Deprecated

- Taxpayer options changed (Don't use the following values, use this instead: TaxPayer::TAXPAYER_HAS_TAXNUMBER)
    - src/szamlaagent/TaxPayer.php

````
    const TAXPAYER_JOINT_VENTURE = 5;
    const TAXPAYER_INDIVIDUAL_BUSINESS = 4;
    const TAXPAYER_PRIVATE_INDIVIDUAL_WITH_TAXNUMBER = 3;
    const TAXPAYER_OTHER_ORGANIZATION_WITH_TAXNUMBER = 2;
    const TAXPAYER_PRIVATE_INDIVIDUAL = -2;
    const TAXPAYER_OTHER_ORGANIZATION_WITHOUT_TAXNUMBER = -3;
````

## [2.9.5] - 2020.07.15

### Added

- added new TaxPayer interface options (added 6 and 7 value)
    - src/szamlaagent/TaxPayer.php
    
````
    const TAXPAYER_NON_EU_ENTERPRISE = 7;
    const TAXPAYER_EU_ENTERPRISE = 6;
````

- Automatic cookie creation for billing accounts per API key (creates a completely unique cookie based on agent key).
    - src/szamlaagent/SzamlaAgent.php (buildCookieFileName)
    
## [2.9.4] - 2020.07.08

### Added

- added new TaxPayer tag
    - src/szamlaagent/Buyer.php
    
## [2.9.3] - 2020.06.24

### Added

- added new function: custom NAV errorcodes handing (TaxPayer interface - query and convert to JSON)
    - src/szamlaagent/SzamlaAgentUtil.php
    
````
    $data = SzamlaAgentAPI::create('agentApiKey')->getTaxPayer('12345678')->toJson();
    $data = SzamlaAgentAPI::create('agentApiKey')->getTaxPayer('12345678')->getDataObj();
````

### Fixed

- taxpayerData property duplication bugfix (TaxPayer interface)
- SimpleXMLExtended XML parser nullpointer fix
    - src/szamlaagent/SimpleXMLExtended.php (removeChild)

## [2.9.2] - 2020.06.19

### Changed

- update TaxPayer interface (custom namespace handing)
    - src/szamlaagent/SzamlaAgentUtil.php
    - src/szamlaagent/SimpleXMLExtended.php
    - src/szamlaagent/response/SzamlaAgentResponse.php

## [2.9.1] - 2020.06.17

### Added

- added new function: custom request-header setting
    - src/szamlaagent/SzamlaAgent.php (addCustomHTTPHeader)
    - src/szamlaagent/SzamlaAgentRequest.php

## [2.9.0] - 2020.06.10

### Added

- added new function: custom invoice template setting (retro, 8 cm, traditional, etc.)
    - src/szamlaagent/header/InvoiceHeader.php
    - src/szamlaagent/header/ReverseInvoiceHeader.php
    - src/szamlaagent/document/invoice/Invoice.php
    - src/szamlaagent/response/InvoiceResponse.php
    - src/szamlaagent/response/ReceiptResponse.php
    - src/szamlaagent/response/SzamlaAgentResponse.php
    - src/szamlaagent/response/ProformaDeletionResponse.php
    
````
    $invoice->getHeader()->setInvoiceTemplate(Invoice::INVOICE_TEMPLATE_8CM);
````

- added new function: failed invoice notification sending handling
    - src/szamlaagent/response/SzamlaAgentResponse.php
  
````
    // successful response handing
    if (​ $result->isSuccess()​ ) {
        echo 'A számla sikeresen elkészült. Számlaszám: ' . $result->getDocumentNumber();
    }
    // if notification sending fails, we can handle it
    if (​ $result->hasInvoiceNotificationSendError()​ ) {
        var_dump($result->getDataObj());
    }
````

## [2.8.4] - 2020.05.21

### Added

- added new function: certification filename setting
   - src/szamlaagent/SzamlaAgent.php (setCertificationFileName)

### Changed

- changed TaxPayer interface (NAV data structure changed)
    - src/szamlaagent/response/TaxPayerResponse.php

## [2.8.3] - 2020.05.06

### Changed

- setting a default path for generated files (xmls, pdf, logs, attachments)
    - src/szamlaagent/SzamlaAgentUtil.php (setBasePath)
- the request connection function has been optimized
    - src/szamlaagent/SzamlaAgentRequest.php (checkConnection)
    
### Removed

- removed function: setting request calling type from session (PHP v1.0)
    - src/szamlaagent/SzamlaAgentRequest.php

## [2.8.2] - 2019.12.26

### Added

- added new function: setting logging path and filename
    - src/szamlaagent/Log.php
    - src/szamlaagent/SzamlaAgent.php
    
## [2.8.1] - 2019.11.20

### Fixed

- fixed cookie management bug (custom cookie filename setting)
    - src/szamlaagent/SzamlaAgentRequest.php
    
## [2.8.0] - 2019.10.24

### Added

- added new functions: buyer ledger invoices settlement period
    - src/szamlaagent/BuyerLedger.php
    
## [2.7.0] - 2019.09.04

### Added

- added new function: attach files to an invoice (maximum 5)
    - src/szamlaagent/SzamlaAgent.php
    - src/szamlaagent/SzamlaAgentUtil.php
    - src/szamlaagent/SzamlaAgentRequest.php
    - src/szamlaagent/SzamlaAgentException.php
    - src/szamlaagent/document/invoice/Invoice.php
    - examples/document/invoice/create_invoice_with_attachment.php
    
## [2.6.0] - 2019.08.28

### Added

- added new interfaces: "getInvoiceData" and "getReceiptData" (xmlszamlaxml XML interface update)
    - examples/document/invoice/get_invoice_data.php
    - examples/document/invoice/get_invoice_pdf.php
- Nyugta adatok lekérdezése
    - examples/document/receipt/get_receipt_data.php
    
## [2.5.0] - 2019.07.25

### Added

- added SzamlaAgent API key interface
    - src/szamlaagent/SzamlaAgent.php
    - src/szamlaagent/SzamlaAgentAPI.php
    - src/szamlaagent/SzamlaAgentRequest.php
    - src/szamlaagent/SzamlaAgentSetting.php
    - src/szamlaagent/document/invoice/Invoice.php
    
## [2.4.0] - 2019.07.25

### Changed

- changed autoload function (import only from 'SzamlaAgent' namespace)
  - examples/autoload.php
  - src/szamlaagent/SzamlaAgent.php
  - examples/document/receipt/create_receipt_with_default_data.php
  - examples/document/receipt/create_receipt_with_custom_data.php
  
- logging optimized (easier search)
  - src/szamlaagent/Log.php

- changed the invoice response customer account url decoding
  - src/szamlaagent/response/InvoiceResponse.php
  
### Fixed

- fixed documentation bug (cookie management)

## [2.3.0] - 2019.06.26

### Changed

- changed exception handling
  - src/szamlaagent/response/SzamlaAgentResponse.php
  
## [2.2.0] - 2019.06.05

### Added

- added new function: create proforma invoice with ordernumber
  - src/szamlaagent/header/InvoiceHeader.php
  
## [2.1.0] - 2019.05.21

### Added

- added PHP API logging
  - src/szamlaagent/SzamlaAgent.php
   
## [2.0.0] - 2019.03.20

### Added

- PHP API package created